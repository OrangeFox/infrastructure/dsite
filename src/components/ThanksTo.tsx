import { FormattedMessage } from 'react-intl';
import { OpenOutside } from './Open-Outside';

const ThanksTo = () => {
  return (
    <span>
      <OpenOutside className='link' href="https://wiki.orangefox.tech/en/credits">
      <FormattedMessage
        id='footer.thanksTo'
        defaultMessage='Thanks to our sponsors <3'
      />
      </OpenOutside>
    </span>
  );
};

export { ThanksTo };
